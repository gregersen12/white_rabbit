<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        // Theres a solution for this that's way smarter, probably reminiscent of the good ol' cashback algorhitm problem. I just don't know it :-(

        if($amount == 0) {
            return array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
            );
        } else {
            // Define empty array to append number and count to
            $coins = array();
            $remainder = 0;

            // We take the manual approach, by using modulus and setting a remainder after division is done.
            if ($amount % 100 != 0) {
                $remainder = $amount % 100;
                array_push($coins, array("100" => 1));
            }

            if ($amount % 50 != 0) {
                $remainder = $amount % 50;
                array_push($coins, array("50" => 1));
            }

            if ($amount % 20 != 0) {
                $remainder = $amount % 20;
                array_push($coins, array("20" => 1));
            }

            if ($amount % 10 != 0) {
                $remainder = $amount % 10;
                array_push($coins, array("10" => 1));
            }

            if ($amount % 5 != 0) {
                $remainder = $amount % 5;
                array_push($coins, array("5" => 1));
            }

            if ($amount % 2 != 0) {
                $remainder = $amount % 2;
                array_push($coins, array("2" => 1));
            }

            if ($amount % 1 != 0) {
                $remainder = $amount % 1;
                array_push($coins, array("1" => 1));
            }

            return $coins;
        }
    }
}