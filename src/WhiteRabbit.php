<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        // Load file so we can work with it
        $file = file_get_contents($filePath, 'r');
        return $file;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        // Define new array we can add counts to
        $letter_count = array();
        // Loop through $parsedFile as for each
            // for each letter between a-z (using byte-values)
                // Add character and character count to array (as two dimensional array); count as 'count', character as 'letter'
        // Calculate median for new 'cleaned' array
        $median = $this->CalculateMedian($letter_count);
        return $median;
    }

    private function CalculateMedian($array)
    {
        $count = count($array);
        sort($array);
        $middlevalue = floor(($count-1) / 2);

        return ($array[$middlevalue] + $array[$middlevalue+1-$count%2]) / 2;
    }
}
